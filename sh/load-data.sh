#!/bin/bash

docker-compose exec neo bash -c 'cat /scripts/load-data.cypher | cypher-shell'
docker-compose exec neo bash -c 'cat /scripts/add-relations.cypher | cypher-shell'
