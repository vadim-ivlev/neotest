# neotest

Command line: `docker-compose exec neo cypher-shell`

GUI:  <http://localhost:7474/browser/>


```sql
// привязать изображения к постам
MATCH (p:Post),(i:Image) 
WHERE  p.id = i.post_id
CREATE (p)<-[r:ILLUSTRATE]-(i)
set p:Illustrated
;


```

